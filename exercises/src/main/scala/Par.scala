//import java.util.concurrent.atomic.AtomicReference
//import java.util.concurrent.{ExecutorService, Callable}
//
//object Par {
//  type Par[A] = ExecutorService => Future[A]
//  def unit[A](a : A) : Par[A] (es : ExecutorService) => UnitFuture(a)
//  def map2[A, B, C] (a : Par[A], b : Par[B])(f : (A,B) => C) : Par[C]
//
//
//
//  private case class UnitFuture[A] ( get : A ) extends Future[A]  {
//    def isDone = true
//    def get(timeout : Long, units : TimeUnit) = get
//    def isCancelled = false
//    def cancel(evenIfRunning : Boolean) : Boolean = false
//  }
//
//  def map2[A,B,C](a : Par[A], b : Par[B])(f : (A,B) => C) : Par[C] =
//    (es : ExecutorService) => {
//      val (af, bf) = (a(es), b(es))
//      Map2Future(af, bf, f)
//    }
//
//  case class Map2Future[A,B,C](a: Future[A], b:Future[B], f : (A,B) => C) extends Future[C] {
//    @volatile var cache : Option[C]
//    def isDone = cache.isDefined
//    def isCancelled = a.isCancelled || b.isCancelled
//    def cancel(evenIfRunning : Boolean) =
//      a.cancel(evenIfRunning) || b.cancel(evenIfRunning)
//  }
//
//  //map(unit(x))(f) == unit(f(x))
//  //map(y)(id) == y
//
//
//  // this implementation of forks the recursive step off to a new logical thread, making it effectively tail recursive.
//  // Howerver we are constructing a right-nested parallel program and we can get performance by dividing the list in hal and running both halves
//  // in parallel
//
//  def sequeunce[A](as : List[Par[A]]) : Par[List[A]] =
//    as match {
//      case Nil => unit(as)
//      case h :: t => map2(h, fork(sequeunce(t)))(_ :: _)
//    }
//
//
//  def run[A](es : ExecutorService) (p : Par[A]) : A = {
//    val ref = new AtomicReference[A]
//    val latch = new CountDownLatch(1)
//    p(es) { a => ref.set(a); latch.countDown }
//    latch.await
//    ref.get
//  }
//
//  def unit[A](a : A) : Par[A] =
//    es =>  new Future[A] {
//      def apply(cb : A => Unit) : Unit =
//        cb(a)
//    }
//
//  def fork[A](a: => Par[A]) : Par[A] =
//    es => new Future[A] {
//      def apply(cb : A => Unit) : Unit =
//        eval(es)(a(es)(cb))
//    }
//
//
//  def eval(es : ExecutorService) (r : => Unit) : Unit =
//    es.submit( new Callable[Unit]) { def call = r }
//
//  def choice[A](cond : Par[Boolean]) (t : Par[A], f : Par[A]) : Par[A] =
//    es =>
//      if (run(es)(cond).get) t(es)
//      else f(es)
//
//
//  def choiceN(n : Par[Int])(choices : List[Par[A]]) : Par[A] =
//    es => {
//      val num = run(es)(n).get
//      choices(num)
//    }
//
//  def choiceConverter(a : Par[Boolean])(f : Boolean => Par[Int]) : Par[Int] =
//    es => {
//      val res = run(es)(a).get
//      f(res)
//    }
//
//  //def choiceViaChoiceN(cond : Par[Boolean])(t: Par[A], f : Par[A]) : Par[A] =
//  //	choiceN(choiceConverter(cond)(p => if(p) unit(0) else unit(1)))(List(t, f))
//
//  def choiceViaChoiceN(cond : Par[Boolean])(ifTrue : Par[A], ifFalse : Par[A]) : Par[A] =
//    choiceN(map(cond)(res => if (res) 0 else 1)(List(ifTrue, ifFalse))
//
//  //def sequenceBalanced[A](as : IndexedSeq[Par[A]]) : Par[IndexedSeq[A]] = fork {
//
//  //	}
//}