package fpinscala.parsing

import fpinscala.testing.{Prop, Gen}

import language.higherKinds

//
//trait Parsers[Parser[+_]] { self => // so inner classes may call methods of trait
//
//  case class ParserOps[A](p: Parser[A]) {
//
//
//  }
//
//  object Laws {
//  }
//}

trait Parsers[ParseError, Parser[+ _]] {
  self =>
  def run[A](p: Parser[A])(input: String): Either[ParseError, A]


  implicit def string(s: String): Parser[String]

  implicit def operators[A](p: Parser[A]) = ParserOps[A](p)

  implicit def asStringParser[A](a: A)(implicit f: A => Parser[String]):
  ParserOps[String] = ParserOps(f(a))

  def or[A](p1: Parser[A], p2: => Parser[A]): Parser[A]

  def many[A, B](a: Parser[A])(f: A => B): Parser[B]

//  def map[A, B](a: Parser[A])(f: A => B): Parser[B]

  //run slice(('a'|'b')).many))("aaba")
  def slice[A](p: Parser[A]): Parser[String]

  def many1[A](p: Parser[A]): Parser[List[A]]

  def product[A, B](p: Parser[A], p2: Parser[B]): Parser[(A, B)]
  def flatMap[A,B](p: Parser[A])(f: A => Parser[B]): Parser[B]

  /*
  These can be implemented using a for-comprehension, which delegates to the `flatMap` and `map` implementations we've provided on `ParserOps`, or they can be implemented in terms of these functions directly.
  */
  def product[A,B](p: Parser[A], p2: => Parser[B]): Parser[(A,B)] =
    flatMap(p)(a => map(p2)(b => (a,b)))
//
//  def map2[A,B,C](p: Parser[A], p2: => Parser[B])(f: (A,B) => C): Parser[C] =
//    for { a <- p; b <- p2 } yield f(a,b)

  def map[A,B](a: Parser[A])(f: A => B): Parser[B] =
    flatMap(a)(f andThen succeed)

  /*
   * A default `succeed` implementation in terms of `string` and `map`.
   * We leave `succeed` abstract, since `map` is defined below in terms of
   * `flatMap` and `succeed`, which would be a circular definition! But we include
   * the definition here in case implementations wish to use it
   * (say if they provide a custom implementation of `map`, breaking the cycle)
   */
//  def defaultSucceed[A](a: A): Parser[A] =
//    string("") map (_ => a)
//
  def succeed[A](a: A): Parser[A]
//
//  def many[A](p: Parser[A]): Parser[List[A]] =
//    map2(p, many(p))(_ :: _) or succeed(List())
//
//  def map21[A,B,C](p: Parser[A], p2: => Parser[B])(f : (A,B) => C): Parser[C] =
//  product(p, p2) map (f.tupled)

//  def product[A,B](p: Parser[A], p2 : => Parser[B]) : Parser[(A,B)]

  //  object Laws {
  //    def equal[A](p1: Parser[A], p2: Parser[A])(in: Gen[String]): Prop =
  //      forAll(in)(s => run(p1)(s) == run(p2)(s))
  //
  //    def mapLaw[A](p: Parser[A])(in: Gen[String]): Prop =
  //      equal(p, p.map(a => a))(in)
  //  }


  case class ParserOps[A](p: Parser[A]) {
    def |[B >: A](p2: => Parser[B]): Parser[B] = self.or(p, p2)

    // use `self` to explicitly disambiguate reference to the `or` method on the `trait`
    def or[B >: A](p2: => Parser[B]): Parser[B] = self.or(p, p2)
  }


  //  val numA : Parser[Int] = char('a').many.map(_.size)
}

object Parsers {
  //  val numA : Parser[Int] =
  //  def char(c: Char): Parser[Char] =
  //    string(c.toString) map (_charAt(0))

}

object HelloWorld {


}

case class Location(input: String, offset: Int = 0) {

  lazy val line = input.slice(0, offset + 1).count(_ == '\n') + 1
  lazy val col = input.slice(0, offset + 1).reverse.indexOf('\n')

  def toError(msg: String): ParseError =
    ParseError(List((this, msg)))

  //  def ad{vanceBy(n: Int) = copy(offset = offset+n)

  /* Returns the line corresponding to this location */
  def currentLine: String =
    if (input.length > 1) input.lines.drop(line - 1).next
    else ""
}

case class ParseError(stack: List[(Location, String)] = List(),
                      otherFailures: List[ParseError] = List()){

}