//package fpinscala.monoids
//
//import fpinscala.testing._
//import fpinscala.parallelism.Nonblocking._
//import fpinscala.parallelism.Nonblocking.Par.toParOps
//
//// infix syntax for `Par.map`, `Par.flatMap`, etc
//import language.higherKinds
//
//trait Monoid[A] {
//  def op(a1: A, a2: A): A
//
//  def zero: A
//}
//
//object Monoid {
//
//  val stringMonoid = new Monoid[String] {
//    def op(a1: String, a2: String) = a1 + a2
//
//    val zero = ""
//  }
//
//  def listMonoid[A] = new Monoid[List[A]] {
//    def op(a1: List[A], a2: List[A]) = a1 ++ a2
//
//    val zero = Nil
//  }
//
//
//  val intAddition: Monoid[Int] = new Monoid[Int] {
//    override def op(a1: Int, a2: Int): Int = a1 + a2
//
//    override def zero: Int = 0
//  }
//
//  def concatenate[A](as: List[A], m: Monoid[A]): A =
//    as.foldLeft(m.zero)(m.op)
//
//  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B = {
//    val bs = as.map(f)
//    bs.foldLeft(m.zero)(m.op)
//  }
//
//  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
//    as.foldLeft(m.zero)((b, a) => m.op(b, f(a)))
//
//  def foldMapV[A, B](as: IndexedSeq[A], m: Monoid[B])(f: A => B): B =
//    if (as.length == 0)
//      m.zero
//    else if (as.length == 1)
//      f(as(0))
//    else {
//      val (l, r) = as.splitAt(as.length / 2)
//      m.op(foldMapV(l, m)(f), foldMapV(r, m)(f))
//    }
//
//  //ability to lift a monoid to operate within certain context (PAR)
//  import fpinscala.parallelism.Nonblocking._
//  def par[A](m: Monoid[A]) : Monoid[Par[A]] = new Monoid[Par[A]] {
//    def zero = Par.unit(m.zero)
//    def op(a : Par[A], b : Par[A]) = a.map2(b)(m.op)
//  }
//
//
//  val booleanOr: Monoid[Boolean] = sys.error("todo")
//
//
//  val booleanAnd: Monoid[Boolean] = sys.error("todo")
//
//  //  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option]{
//  //    override def op(a1: Option, a2: Option): Option = a1 orElse(a2)
//  //    override def zero: Option = None
//  //  }
//
//  def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
//    override def op(a1: Option[A], a2: Option[A]): Option[A] = a1 orElse (a2)
//
//    override def zero: Option[A] = None
//  }
//
//  def endoMonoid[A]: Monoid[A => A] = new Monoid[(A) => A] {
//    override def op(f: A => A, g: A => A): A => A = f.compose(g)
//
//    override def zero = (a: A) => a
//  }
//
//
//  def monoidLaws[A](m: Monoid[A], gen: Gen[A]): Prop =
//
//
//  // TODO: Placeholder for `Prop`. Remove once you have implemented the `Prop`
//  // data type from Part 2.
////  trait Prop {}
////
//  // TODO: Placeholder for `Gen`. Remove once you have implemented the `Gen`
//  // data type from Part 2.
//
////  import fpinscala.testing._
////  import Prop._
//
//  def monoidLaws[A](m: Monoid[A], gen: Gen[A]): Prop = sys.error("todo")
//
//  def trimMonoid(s: String): Monoid[String] = sys.error("todo")
//
//  def concatenate[A](as: List[A], m: Monoid[A]): A =
//    sys.error("todo")
//
//  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
//    sys.error("todo")
//
//  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B =
//    sys.error("todo")
//
//  def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B =
//    sys.error("todo")
//
//  def foldMapV[A, B](as: IndexedSeq[A], m: Monoid[B])(f: A => B): B =
//    sys.error("todo")
//
//  def ordered(ints: IndexedSeq[Int]): Boolean =
//    sys.error("todo")
//
//  sealed trait WC
//
//  case class Stub(chars: String) extends WC
//
//  case class Part(lStub: String, words: Int, rStub: String) extends WC
//
//  def par[A](m: Monoid[A]): Monoid[Par[A]] =
//    sys.error("todo")
//
//  def parFoldMap[A, B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): Par[B] =
//    sys.error("todo")
//
//  val wcMonoid: Monoid[WC] = new Monoid[WC] {
//    override def op(a1: WC, a2: WC): WC = (a1, a2) match {
//      case (Stub(c), Stub(d)) => Stub(c+d)
//    }
//    override def zero: WC =  Stub("")
//  }
//
//  def count(s: String): Int = sys.error("todo")
//
//  def productMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[(A, B)] =
//    sys.error("todo")
//
//  def functionMonoid[A, B](B: Monoid[B]): Monoid[A => B] =
//    sys.error("todo")
//
//  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[Map[K, V]] =
//    sys.error("todo")
//
//  def bag[A](as: IndexedSeq[A]): Map[A, Int] =
//    sys.error("todo")
//}
//
//trait Foldable[F[_]] {
//
//  import Monoid._
//
//  def foldRight[A, B](as: F[A])(z: B)(f: (A, B) => B): B =
//    sys.error("todo")
//
//  def foldLeft[A, B](as: F[A])(z: B)(f: (B, A) => B): B =
//    sys.error("todo")
//
//  def foldMap[A, B](as: F[A])(f: A => B)(mb: Monoid[B]): B =
//    sys.error("todo")
//
//  def concatenate[A](as: F[A])(m: Monoid[A]): A =
//    sys.error("todo")
//
//  def toList[A](as: F[A]): List[A] =
//    sys.error("todo")
//}
//
//object ListFoldable extends Foldable[List] {
//  override def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B) =
//    sys.error("todo")
//
//  override def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B) =
//    sys.error("todo")
//
//  override def foldMap[A, B](as: List[A])(f: A => B)(mb: Monoid[B]): B =
//    sys.error("todo")
//}
//
//object IndexedSeqFoldable extends Foldable[IndexedSeq] {
//  override def foldRight[A, B](as: IndexedSeq[A])(z: B)(f: (A, B) => B) =
//    sys.error("todo")
//
//  override def foldLeft[A, B](as: IndexedSeq[A])(z: B)(f: (B, A) => B) =
//    sys.error("todo")
//
//  override def foldMap[A, B](as: IndexedSeq[A])(f: A => B)(mb: Monoid[B]): B =
//    sys.error("todo")
//}
//
//object StreamFoldable extends Foldable[Stream] {
//  override def foldRight[A, B](as: Stream[A])(z: B)(f: (A, B) => B) =
//    sys.error("todo")
//
//  override def foldLeft[A, B](as: Stream[A])(z: B)(f: (B, A) => B) =
//    sys.error("todo")
//}
//
//sealed trait Tree[+A]
//
//case class Leaf[A](value: A) extends Tree[A]
//
//case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
//
//object TreeFoldable extends Foldable[Tree] {
//  override def foldMap[A, B](as: Tree[A])(f: A => B)(mb: Monoid[B]): B =
//    sys.error("todo")
//
//  override def foldLeft[A, B](as: Tree[A])(z: B)(f: (B, A) => B) =
//    sys.error("todo")
//
//  override def foldRight[A, B](as: Tree[A])(z: B)(f: (A, B) => B) =
//    sys.error("todo")
//}
//
//object OptionFoldable extends Foldable[Option] {
//  override def foldMap[A, B](as: Option[A])(f: A => B)(mb: Monoid[B]): B =
//    sys.error("todo")
//
//  override def foldLeft[A, B](as: Option[A])(z: B)(f: (B, A) => B) =
//    sys.error("todo")
//
//  override def foldRight[A, B](as: Option[A])(z: B)(f: (A, B) => B) =
//    sys.error("todo")
//}

