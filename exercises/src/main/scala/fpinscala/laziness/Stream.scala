package fpinscala.laziness

import Stream._

import scala.annotation.tailrec

trait Stream[+A] {

  def foldRight[B](z: => B)(f: (A, => B) => B): B = // The arrow `=>` in front of the argument type `B` means that the function `f` takes its second argument by name and may choose not to evaluate it.
    this match {
      case Cons(h, t) => f(h(), t().foldRight(z)(f)) // If `f` doesn't evaluate its second argument, the recursion never occurs.
      case _ => z
    }

  def exists2(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exists(p)
    case _ => false
  }

  def takeWhile2(p: A => Boolean): Stream[A] =
    foldRight(empty[A])((h, t) => if (p(h)) cons(h, t) else empty)

  def exists(p: A => Boolean): Boolean =
    foldRight(false)((h, t) => p(h) || t) // Here `b` is the unevaluated recursive step that folds the tail of the stream. If `p(a)` returns `true`, `b` will never be evaluated and the computation terminates early.

  @annotation.tailrec
  final def find(f: A => Boolean): Option[A] = this match {
    case Empty => None
    case Cons(h, t) => if (f(h())) Some(h()) else t().find(f)
  }

  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if (n > 1) => cons(h(), t() take n - 1)
    case Cons(h, _) if n == 1 => cons(h(), empty)
    case _ => empty
  }

  // Unlike take drop is incremental so it must transverse the elems
  // of the streams eagerly ( what I did initially)
  def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if (n > 0) => t() drop (n - 1)
    case _ => this
  }

  def headOptionPatternMatch: Option[A] = this match {
    case Empty => None
    case Cons(h, t) => Some(h())
  }

  //this implementation will stackoverflow for large numbers and it is not tail recursive
  def toLiStRecrusive: List[A] = this match {
    case Cons(h, t) => h() :: t().toLiStRecrusive
    case _ => List()
  }

  def toList: List[A] = {
    @tailrec
    def go(s: Stream[A], acc: List[A]): List[A] = s match {
      case Empty => acc
      case Cons(h, t) => go(t(), h() :: acc)
    }
    go(this, List()).reverse
  }


  def toListBuff: List[A] = {
    import collection.mutable.ListBuffer
    val buff = new ListBuffer[A]
    @tailrec
    def go(s: Stream[A]): List[A] = s match {
      case Cons(h, t) => buff += h(); go(t());
      case _ => buff.toList
    }
    go(this)
  }

  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h()) => cons(h(), t() takeWhile (p))
    case _ => empty

  }

  // via foldright
  def forAll(p: A => Boolean): Boolean =
    foldRight(true)((a, b) => p(a) && b)

  def headOption: Option[A] =
    foldRight(None: Option[A])((h, t) => Some(h))

  // 5.7 map, filter, append, flatmap using foldRight. Part of the exercise is
  // writing your own function signatures.

  //  def map[B](f : A => B) : Stream[B] = this match {
  //    case Cons(h, t) => cons(f(h()), t.map(f))
  //    case _ => empty
  //  }
  //
  def map[B](f: A => B): Stream[B] =
    foldRight(empty[B])((h, t) => cons(f(h), t))


  def filter(f: A => Boolean): Stream[A] =
    foldRight(empty[A])((h, t) => if (f(h)) cons(h, t) else t)

  def append[B >: A](s1: Stream[B]): Stream[B] =
    foldRight(s1)((h, t) => cons(h, t))

  def fmap[B](f: A => Stream[B]): Stream[B] =
    foldRight(empty[B])((h, t) => f(h) append t)

  val fibViaUnfold =
    unfold((0, 1)) { case (f0, f1) => Some((f0, (f1, f1 + f0))) }

//  def mapViaUnfold[A, B](f: A => B): Stream[B] =
//    unfold(this) {
//      case Cons(h, t) => Some((f(h()), t()))
//      case _ => None
//    }

  def zipWith[B,C](s2: Stream[B])(f: (A,B) => C): Stream[C] =
    unfold((this, s2)) {
      case (Cons(h1,t1), Cons(h2,t2)) =>
        Some((f(h1(), h2()), (t1(), t2())))
      case _ => None
    }

  // special case of `zipWith`
  def zip[B](s2: Stream[B]): Stream[(A,B)] =
    zipWith(s2)((_,_))

  def startsWith[B](s: Stream[B]): Boolean = sys.error("todo")

}

case object Empty extends Stream[Nothing]

case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = Stream.cons(1, ones)

  def from(n: Int): Stream[Int] = sys.error("todo")

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some((h, s)) => cons(h, unfold(s)(f))
    case None => empty
  }


}

object HelloStreams {

  def main(args: Array[String]) {
    val a = List(1, 2, 3)
    val stream2 = cons(() => 3, Empty)
    val stream = cons(() => 3, cons(() => 4, Empty))
    System.out.println(stream.toList)
    System.out.println(stream2.toList)
  }
}