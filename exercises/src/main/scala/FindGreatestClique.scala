/**
  * Created by fbrubacher on 6/12/2016.
  */
//
//type Node = Int
//type Nodes = Set[Node]
//type Graph = Map[Node, Nodes]
//type Clique = Nodes
//
//val Cliques = Set(Cliques())
//val Empty = scala.collection.immutable.SortedSet[Node]()
//class FindGreatestClique {
//
//  def bromKarbosch(g : Graph, r: Clique, x : Nodes, p : Nodes) = {
//    if (p.isEmpty && x.isEmpty) Set(r) else {
//      val u = (p | x).maxBy(n => g(n).size)
//      (p diff g(u)).foldLeft((Cliques, p, x)) {
//        case ((rs, p, x), v) => (rs ++ bromKarbosch(g, r + v, p & g(v), x & g(v)), p - v, x + v)
//      }._1
//    }
//  }
//
//  def largestCliques(g: Graph) : Set[Clique] =
//    bromKarbosch(g, Empty, g.keySet, Empty).foldLeft(Cliques)
//    { case (cs, c) =>
//      if (cs.isEmpty || c.size > cs.head.size) Set(c)
//      else if (c.size == cs.head.size) cs + c else cs
//    }


//}
