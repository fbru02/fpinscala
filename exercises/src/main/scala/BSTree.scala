/**
  * Created by fbrubacher on 6/15/2016.
  */
sealed abstract class Tree[+A]

case class Node[A](left: Tree[A], key: A, right: Tree[A]) extends Tree[A]

case object Empty extends Tree[Nothing]

import Ordering.Implicits._
//
//class BinaryTreeMethods[A](val tree: Tree[A]) extends AnyVal {
//  def get(key: A)(implicit cmp: Ordering[A]): Option[Tree[A]] =
//    tree match {
//      case Empty => None
//      case Node(_, key, _) => Some(tree)
//      case Node(left, pivot, _) if key < pivot => left.get(key)
//      case Node(_, _, right) => right.get(key)
//    }
//}
