/**
  * Created by fbrubacher on 7/26/2016.
  */

import fpinscala.state._
import fpinscala.testing.Prop._

object Prop {

  sealed trait Result {
    def isFalsified: Boolean
  }

  case object Passed extends Result {
    def isFalsified = false
  }

  case class Falsified(failure: FailedCase, success: SuccessCount) extends Result {
    def isFalsified = true
  }

}

case class Prop(run : (MaxSize, TestCases, RNG) => Result) {

  def &&(p : Prop) = Prop {
    (max,n,rng) => run(max,n,rng) match {
      case Passed | Proved => p.run(max, n, rng)
      case x => x
    }
  }

  def ||(p : Prop) = Prop {
    (max,n,rng) => run
  }

}


object Gen {
  def listOfN[A](n : Int, g : Gen[A]) : Gen[List[A]] =
      Gen(State.sequence(List.fill(n)(g.sample)))

  def forAll[A](as : Gen[A])(f : A => Boolean) : Prop = Prop {
    (n, rng) => randomStream(as)(rng).zip(Stream.from(0)).take(n).map {
      case (a, i) => try {
        if (f(a)) Passed else Falsified(a.toString(), i)
      } catch { case e : Exception => Falsified(buildMsg(a, e), i) }
    }.find(_.isFalsified).getOrElse(Passed)
  }
}


case class Gen[+A](sample : State[RNG, A]){

    def unit[A](a : => A) : Gen[A]  = Gen(State.unit(a))

    def boolean  : Gen[Boolean] = Gen(State(RNG.boolean))


    def map[B](f : A => B) : Gen[B] =
      Gen(sample.map(f))

    def flatMap[B](f: A => Gen[B]) : Gen[B] =
      Gen(sample.flatMap(a => f(a).sample))

    def listOfN(size: Int): Gen[List[A]] =
      Gen.listOfN(size, this)

    def listOfN(size : Gen[Int]) : Gen[List[A]] =
      size flatMap ( n => this.listOfN(n))
}
